package pl.golebiewski;

public class Filters {

    public static double[][] lowPassFilter =
            {{1.0, 1.0, 1.0},
            {1.0, 1.0, 1.0},
            {1.0, 1.0, 1.0}};

    public static double[][] lowPassPlusFilter =
            {{1.0, 1.0, 1.0},
            {1.0, 12.0, 1.0},
            {1.0, 1.0, 1.0}};

    public static double[][] highPassFilter =
            {{-1.0, -1.0, -1.0},
            {-1.0, 9.0, -1.0},
            {-1.0, -1.0, -1.0}};

    public static double[][] gaussianFilter =
            {{1.0, 2.0, 1.0},
            {2.0, 4.0, 2.0},
            {1.0, 2.0, 1.0}};

    public static double[][] robertsHorizontalFilter =
            {{0.0, 0.0, 0.0},
            {-1.0, 1.0, 0.0},
            {0.0, 0.0, 0.0}};

    public static double[][] robertsVerticalFilter =
            {{0.0, -1.0, 0.0},
            {0.0, 1.0, 0.0},
            {0.0, 0.0, 0.0}};

    public static double[][] sobelHorizontalFilter =
            {{1.0, 2.0, 1.0},
            {0.0, 0.0, 0.0},
            {-1.0, -2.0, -1.0}};

    public static double[][] sobelVerticalFilter =
            {{1.0, 0.0, -1.0},
            {2.0, 0.0, -2.0},
            {1.0, 0.0, -1.0}};

    public static double[][] laplace1Filter =
            {{0.0, -1.0, 0.0},
            {-1.0, 4.0, -1.0},
            {0.0, -1.0, 0.0}};

    public static double[][] laplace2Filter =
            {{1.0, -2.0, 1.0},
            {-2.0, 4.0, -2.0},
            {1.0, -2.0, 1.0}};
}
