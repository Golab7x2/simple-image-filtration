package pl.golebiewski;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    @FXML Button filterButton;
    @FXML Button resetButton;
    @FXML Button loadButton;
    @FXML ToggleGroup radioGroup;
    @FXML ImageView imageView;

    private Image image;
    private int imageHeight;
    private int imageWidth;
    private double[][] redArray;
    private double[][] greenArray;
    private double[][] blueArray;
    private double[][] kernel;
    private double kernelSum;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //Load image and initialize variables
        loadButton.setOnAction(EventHandler -> {

            //Get path to an image and show loaded image
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Image");
            Stage stage = (Stage) loadButton.getScene().getWindow();
            File file = fileChooser.showOpenDialog(stage);

            if(file != null) {

                image = new Image("file:" + file.getPath());
                this.imageView.setImage(image);

                //Initialize arrays and variables
                this.imageHeight = (int) image.getHeight();
                this.imageWidth = (int) image.getWidth();
                PixelReader pixelReader = image.getPixelReader();

                redArray = new double[imageWidth][imageHeight];
                greenArray = new double[imageWidth][imageHeight];
                blueArray = new double[imageWidth][imageHeight];

                //Divide image into R, G, B bands
                for (int i = 0; i < imageWidth; i++) {
                    for (int j = 0; j < imageHeight; j++) {
                        redArray[i][j] = pixelReader.getColor(i, j).getRed();
                        greenArray[i][j] = pixelReader.getColor(i, j).getGreen();
                        blueArray[i][j] = pixelReader.getColor(i, j).getBlue();
                    }
                }
            }
        });

        //Convolution
        filterButton.setOnAction(EventHandler -> {

            if(this.image != null) {

                //Check for filter choice
                RadioButton choice = (RadioButton) this.radioGroup.getSelectedToggle();

                switch (choice.getText()) {
                    case "Low Pass":
                        this.kernel = Filters.lowPassFilter;
                        break;
                    case "Low Pass Plus":
                        this.kernel = Filters.lowPassPlusFilter;
                        break;
                    case "High Pass":
                        this.kernel = Filters.highPassFilter;
                        break;
                    case "Gaussian":
                        this.kernel = Filters.gaussianFilter;
                        break;
                    case "Roberts Horizontal":
                        this.kernel = Filters.robertsHorizontalFilter;
                        break;
                    case "Roberts Vertical":
                        this.kernel = Filters.robertsVerticalFilter;
                        break;
                    case "Sobel Horizontal":
                        this.kernel = Filters.sobelHorizontalFilter;
                        break;
                    case "Sobel Vertical":
                        this.kernel = Filters.sobelVerticalFilter;
                        break;
                    case "Laplace 1":
                        this.kernel = Filters.laplace1Filter;
                        break;
                    case "Laplace 2":
                        this.kernel = Filters.laplace2Filter;
                        break;
                }

                //Count sum of kernel values
                this.kernelSum = 0;
                for (double k[] : kernel) {
                    for (double l : k) {
                        this.kernelSum += l;
                    }
                }
                if (kernelSum < 0)
                    kernelSum = 1;

                //Create new arrays for filtered values
                double[][] redArrayFiltered = new double[imageWidth][imageHeight];
                double[][] greenArrayFiltered = new double[imageWidth][imageHeight];
                double[][] blueArrayFiltered = new double[imageWidth][imageHeight];

                WritableImage imageFiltered = new WritableImage(imageWidth, imageHeight);
                PixelWriter pixelWriter = imageFiltered.getPixelWriter();

                for (int i = 1; i < this.imageWidth - 1; i++) {
                    for (int j = 1; j < this.imageHeight - 1; j++) {
                        //Red band
                        redArrayFiltered[i][j] = (this.kernel[0][0] * redArray[i - 1][j - 1]
                                + this.kernel[0][1] * redArray[i][j - 1]
                                + this.kernel[0][2] * redArray[i + 1][j - 1]
                                + this.kernel[1][0] * redArray[i - 1][j]
                                + this.kernel[1][1] * redArray[i][j]
                                + this.kernel[1][2] * redArray[i + 1][j]
                                + this.kernel[2][0] * redArray[i - 1][j + 1]
                                + this.kernel[2][1] * redArray[i][j + 1]
                                + this.kernel[2][2] * redArray[i + 1][j + 1]) / this.kernelSum;
                        if (redArrayFiltered[i][j] < 0)
                            redArrayFiltered[i][j] = 0;
                        if (redArrayFiltered[i][j] > 1)
                            redArrayFiltered[i][j] = 1;
                        //Green band
                        greenArrayFiltered[i][j] = (this.kernel[0][0] * greenArray[i - 1][j - 1]
                                + this.kernel[0][1] * greenArray[i][j - 1]
                                + this.kernel[0][2] * greenArray[i + 1][j - 1]
                                + this.kernel[1][0] * greenArray[i - 1][j]
                                + this.kernel[1][1] * greenArray[i][j]
                                + this.kernel[1][2] * greenArray[i + 1][j]
                                + this.kernel[2][0] * greenArray[i - 1][j + 1]
                                + this.kernel[2][1] * greenArray[i][j + 1]
                                + this.kernel[2][2] * greenArray[i + 1][j + 1]) / this.kernelSum;
                        if (greenArrayFiltered[i][j] < 0)
                            greenArrayFiltered[i][j] = 0;
                        if (greenArrayFiltered[i][j] > 1)
                            greenArrayFiltered[i][j] = 1;
                        //Blue band
                        blueArrayFiltered[i][j] = (this.kernel[0][0] * blueArray[i - 1][j - 1]
                                + this.kernel[0][1] * blueArray[i][j - 1]
                                + this.kernel[0][2] * blueArray[i + 1][j - 1]
                                + this.kernel[1][0] * blueArray[i - 1][j]
                                + this.kernel[1][1] * blueArray[i][j]
                                + this.kernel[1][2] * blueArray[i + 1][j]
                                + this.kernel[2][0] * blueArray[i - 1][j + 1]
                                + this.kernel[2][1] * blueArray[i][j + 1]
                                + this.kernel[2][2] * blueArray[i + 1][j + 1]) / this.kernelSum;
                        if (blueArrayFiltered[i][j] < 0)
                            blueArrayFiltered[i][j] = 0;
                        if (blueArrayFiltered[i][j] > 1)
                            blueArrayFiltered[i][j] = 1;

                        pixelWriter.setColor(i, j, Color.color(redArrayFiltered[i][j], greenArrayFiltered[i][j], blueArrayFiltered[i][j]));
                    }
                }

                imageView.setImage(imageFiltered);
            }
        });

        resetButton.setOnAction(EventHandler -> {
            if(this.image != null)
                this.imageView.setImage(this.image);
        });

    }
}
