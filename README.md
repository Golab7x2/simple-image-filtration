*Program stworzony w ramach projektu z Zaawansowanych metod opracowań obserwacji*

Simple Image Filtration pozwala na przeprowadzanie filtracji w dziedzinie przestrzennej. Zaimplementowane zostały podstawowe maski filtrów dolno- i górnoprzepustowych, filtr Gaussa, filtry wykrywające krawędzie.

Program wykorzystuje jedynie API języka Java (bez dodatkowych bibliotek przystosowanych do obróbki obrazu).